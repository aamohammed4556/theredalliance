# The Red Alliance

Localhost FRC scouting application that works over ethernet.

## Devlopment

Currently, I am rewriting the previous implementation in Vue and with Typescript.
The old write is not fully complete either.

### Running

- To run the package during developement, use `./run`

### Goals

1. Get a backend that can handle data entered by users
2. Create profiles for each recorded team
3. Last should be an easy to use front end
